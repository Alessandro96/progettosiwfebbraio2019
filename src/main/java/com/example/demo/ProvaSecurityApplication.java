package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaSecurityApplication.class, args);
	}
}
