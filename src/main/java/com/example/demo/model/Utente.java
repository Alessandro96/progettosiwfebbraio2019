package com.example.demo.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "TIPOLOGIA_UTENTE")
public class Utente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Long id;
	
	@Column
	private int active;
	
	@Column
	private String email;
	
	@Column
	private String cognome;
	
	@Column(unique=true)
	private String nome;
	
	@Column
	private String password;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	//@JoinColumn(name="tabella_join")
	@Column
	private Set<Ruolo> ruoli;

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
	
	public Set<Ruolo> getRuoli() {
		return ruoli;
	}

	public void setRuoli(Set<Ruolo> ruolo) {
		this.ruoli = ruolo;
	}

	public Utente(int active, String email, String cognome, String nome, String password, Set<Ruolo> ruolo) {
		this.active = active;
		this.email = email;
		this.cognome = cognome;
		this.nome = nome;
		this.password = password;
		this.ruoli = ruolo;
	}

	public Utente(Utente utente) {
		this.active=utente.getActive();
		this.cognome=utente.getCognome();
		this.email=utente.getEmail();
		this.nome=utente.getNome();
		this.password=utente.getPassword();
		this.ruoli=utente.getRuoli();
	}
	
	public Utente() {}
	
}
