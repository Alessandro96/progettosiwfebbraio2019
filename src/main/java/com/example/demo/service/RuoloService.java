package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Ruolo;
import com.example.demo.repository.RuoloRepository;

@Service
public class RuoloService {
	
	@Autowired
	private RuoloRepository rep;
	
	public Ruolo salvaRuolo(Ruolo ruolo) {
		return this.rep.save(ruolo);
	}
	
}
