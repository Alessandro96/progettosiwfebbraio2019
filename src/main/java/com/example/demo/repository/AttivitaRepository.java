package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Attivita;

public interface AttivitaRepository extends JpaRepository<Attivita, Long> {
	Optional<Attivita> findByNome(String nome);
}
