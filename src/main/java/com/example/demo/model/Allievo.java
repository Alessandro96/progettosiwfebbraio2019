package com.example.demo.model;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Allievo extends Utente{
	
	@Column
	private String cittaNatale;
	
	@Column
	private String dataDiNascita;
	
	@Column
	private Long telefono;
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Attivita> attivita;
	
	public String getCittaNatale() {
		return cittaNatale;
	}

	public void setCittaNatale(String cittaNatale) {
		this.cittaNatale = cittaNatale;
	}

	public String getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public Long getTelefono() {
		return telefono;
	}

	public void setTelefono(Long telefono) {
		this.telefono = telefono;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

	public Allievo (int active, String cognome, String nome, Set<Ruolo> ruoli, String cittaNatale, String email, String password,
						 String dataDiNascita, Long telefono, List<Attivita> attivita) {
		super(active, email, cognome, nome, password, ruoli);
		this.telefono=telefono;
		this.cittaNatale=cittaNatale;
		this.dataDiNascita=dataDiNascita;
		this.attivita=attivita;
	}
	
	public Allievo(Allievo allievo) {
		super(new Utente(allievo.getActive(), allievo.getEmail(), allievo.getCognome(), 
				allievo.getNome(), allievo.getPassword(), allievo.getRuoli()));
		this.cittaNatale=allievo.getCittaNatale();
		this.dataDiNascita=allievo.getDataDiNascita();
		this.telefono=allievo.getTelefono();
		this.attivita=allievo.getAttivita();
	}
	
	public Allievo() {};
}
