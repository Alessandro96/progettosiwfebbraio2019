package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Centro;

public interface CentroRepository extends JpaRepository<Centro, Long>{

}
