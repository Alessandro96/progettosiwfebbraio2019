package com.example.demo.model;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Attivita {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	@Column
	private Long id;
	@Column(unique=true)
	private String nome;
	@Column
	private String data;
	@Column
	private String ora;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy=("attivita"))
	private List<Allievo> allievi;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getOra() {
		return ora;
	}
	public void setOra(String ora) {
		this.ora = ora;
	}
	public List<Allievo> getAllievi(){
		return this.allievi;
	}
	public void setAllievi(List<Allievo> allievi) {
		this.allievi=allievi;
	}
	
	public Attivita() {	}
	
	public Attivita(String nome, String data, String ora, List<Allievo> allievi) {
		this.nome = nome;
		this.data = data;
		this.ora = ora;
		this.allievi=allievi;
	}
	
	
}
