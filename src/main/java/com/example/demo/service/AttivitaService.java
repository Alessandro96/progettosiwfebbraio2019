package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Attivita;
import com.example.demo.repository.AttivitaRepository;

@Service
public class AttivitaService {

	@Autowired
	AttivitaRepository repo;
	
	public Attivita salvaAttivita(Attivita attivita) {
		return repo.save(attivita);
	}
	
	public List<Attivita> cercaTutte(){
		return this.repo.findAll();
	}
	
	public Attivita cercaAttivita(String nome) {
		Optional<Attivita> optional = this.repo.findByNome(nome);
		optional.orElseThrow(() -> new UsernameNotFoundException("attivita non trovata"));
		return optional.get();
	}
	
	public void eliminaAttivita(Attivita attivita) {
		this.repo.delete(attivita);
	}
}
