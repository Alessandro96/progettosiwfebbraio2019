package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Allievo;

public interface AllievoRepository extends JpaRepository<Allievo, Long>{
	
	Optional<Allievo> findByNome(String nome);
	
}
