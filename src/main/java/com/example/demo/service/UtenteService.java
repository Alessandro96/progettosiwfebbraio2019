package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.CustomUserDetails;
import com.example.demo.model.Utente;
import com.example.demo.repository.UtenteRepository;

import java.util.*;

@Service
public class UtenteService {

		@Autowired
		private UtenteRepository rep;
		
		public Utente salvaUtente(Utente utente) {
			return this.rep.save(utente);
		}
		
		public List<Utente> findAll(){
			return (List<Utente>) this.rep.findAll();
		}
		
		public Utente findByNome(String nome)throws UsernameNotFoundException{
			Optional<Utente> utenteOpzionale=rep.findByNome(nome);
			
			utenteOpzionale.orElseThrow(() -> new UsernameNotFoundException("Nome non trovato"));
			return utenteOpzionale.map(utente -> new Utente(utente)).get();
		}
}
