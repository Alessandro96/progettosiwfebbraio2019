package com.example.demo.model;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails extends Utente implements UserDetails{

	private static final long serialVersionUID = 1L;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return getRuoli()
                .stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getRuolo()))
                .collect(Collectors.toList());
	}

	@Override
	public String getPassword() {
        return super.getPassword();
	}

	@Override
	public String getUsername() {
		return super.getNome();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public CustomUserDetails(final Utente utente) {
		super(utente);
	}
}
