package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Responsabile;
import com.example.demo.repository.ResponsabileRepository;

@Service
public class ResponsabileService {

		@Autowired
		ResponsabileRepository rep;
		
		public Responsabile salvaResponsabile(Responsabile responsabile) {
			return this.rep.save(responsabile);
		}
		
		public List<Responsabile> findAll(){
			return (List<Responsabile>) this.rep.findAll();
		}
		
		public Responsabile findByNome(String nome){
			Optional<Responsabile> responsabileOpzionale=rep.findByNome(nome);
			responsabileOpzionale.orElseThrow(() -> new UsernameNotFoundException("Nome non trovato"));
			return responsabileOpzionale.map(responsabile -> new Responsabile(responsabile)).get();
		}
}
