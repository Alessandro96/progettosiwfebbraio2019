package com.example.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Centro {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Long id;
	@Column(unique=true)
	private String nome;
	@Column
	private String indirizzo;
	@Column
	private String email;
	@Column
	private Long telefono;
	@Column
	private int capienza;
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="centro_ids")
	private List<Attivita> attivita;
	
	public List<Attivita> getAttivita(){
		return this.attivita;
	}
	
	public void setAttivita(List<Attivita> attivita) {
		this.attivita=attivita;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getTelefono() {
		return telefono;
	}
	public void setTelefono(Long telefono) {
		this.telefono = telefono;
	}
	public int getCapienza() {
		return capienza;
	}
	public void setCapienza(int capienza) {
		this.capienza = capienza;
	}
	
	public Centro() {	}
	
	public Centro(String nome, String indirizzo, String email, Long telefono, int capienza, List<Attivita> attivita) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.email = email;
		this.telefono = telefono;
		this.capienza = capienza;
		this.attivita = attivita;
	}
	
	
	
}
