package com.example.demo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Responsabile extends Utente{
	
	@Column
	public String key;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Centro centro;
	
	public String getKey() {
		return this.key;
	}
	
	public void setKey(String key) {
		this.key=key;
	}
	
	public Centro getCentro() {
		return this.centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public Responsabile(int active, String nome, String cognome, String password, String email, Set<Ruolo> ruoli, String key, Centro centro) {
		super(active, email, cognome, nome, password, ruoli);
		this.key=key;
		this.centro=centro;
	}
	
	public Responsabile(Responsabile responsabile) {
		super(new Utente(responsabile.getActive(), responsabile.getEmail(), responsabile.getCognome(), 
					responsabile.getNome(), responsabile.getPassword(),responsabile.getRuoli()));
		this.key=responsabile.getKey();
		this.centro = responsabile.getCentro();
	}
	
	public Responsabile() {}
}
