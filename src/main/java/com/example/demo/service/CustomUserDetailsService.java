package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.CustomUserDetails;
import com.example.demo.model.Responsabile;
import com.example.demo.model.Utente;
import com.example.demo.repository.ResponsabileRepository;
import com.example.demo.repository.UtenteRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UtenteRepository usersRepository;
	
	@Autowired
	private ResponsabileRepository respRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		
		
		Optional<Utente> utenteOpzionale=usersRepository.findByNome(username);
		
		if(utenteOpzionale.isPresent()==false) {
			Optional<Responsabile> responsabileOpzionale=respRepository.findByNome(username);
			responsabileOpzionale.orElseThrow(() -> new UsernameNotFoundException("bitconnect"));
			System.out.println("Presente tra i responsabili: " + responsabileOpzionale.isPresent());	
		}
		
		utenteOpzionale.orElseThrow(() -> new UsernameNotFoundException("bitconnect"));
		return utenteOpzionale.map(utente -> new CustomUserDetails(utente)).get();
	}

}
