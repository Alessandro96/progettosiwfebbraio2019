package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Centro;
import com.example.demo.repository.CentroRepository;

import java.util.*;

@Service
public class CentroService {

	@Autowired
	CentroRepository rep;
	
	public List<Centro> cercaTutti(){
		return rep.findAll();
	}
	
	public Centro salvaCentro(Centro centro) {
		return rep.save(centro);
	}
}
