package com.example.demo.model;

import javax.persistence.*;
import java.util.*;

@Entity
public class Ruolo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Long id;
	
	@Column(unique=true)
	private String ruolo;
	
	public String getRuolo() {
		return this.ruolo;
	}
	
	public void setRuolo(String ruolo) {
		this.ruolo=ruolo;
	}
	
	public Ruolo(String ruolo) {
		this.ruolo=ruolo;
	}
	
	public Ruolo() {}
}
