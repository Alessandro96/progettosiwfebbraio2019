package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Allievo;
import com.example.demo.repository.AllievoRepository;

@Service
public class AllievoService{

	@Autowired
	AllievoRepository repo;
	
	public Allievo cercaPerNome(String nome) {
		Optional<Allievo> optional = repo.findByNome(nome);
		optional.orElseThrow(() -> new UsernameNotFoundException("utente non trovato"));
		return optional.map(allievo -> new Allievo(allievo)).get();
	}
	
	public Allievo salvaAllievo(Allievo allievo) {
		return repo.save(allievo);
	}
	
	public void rimuoviAllievo(Allievo allievo) {
		this.repo.delete(allievo);
	}
}
