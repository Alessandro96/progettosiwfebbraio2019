package com.example.demo.controller;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import java.util.*;

import com.example.demo.model.*;
import com.example.demo.service.*;

@Controller
public class UtenteController {
	@Autowired
	private UtenteService serv;
	
	@Autowired
	private RuoloService servR;
	
	@Autowired
	private ResponsabileService servResp;
	
	@Autowired
	private AllievoService servAll;
	
	@Autowired
	private AttivitaService servAtt;
	
	@Autowired 
	private CentroService servCen;
	
	@RequestMapping("/")
	public String home(Model modello){
		
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(authentication.getName());
		
		if(authentication.getName().compareTo("anonymousUser")==0)
			return "homePage";
		else{		
			Utente utenteLog;
			
			utenteLog = this.serv.findByNome(authentication.getName());
			Iterator<Ruolo> i = utenteLog.getRuoli().iterator();
			Ruolo ruoloLog = i.next();
			
			if(ruoloLog.getRuolo().compareTo("allievo")==0) {
				return homePageAllievo(modello);
			}
				
			else {
				return homePageResponsabile(modello);
			}
		}
	}
	
/*---------------------Link pagine iniziali-------------------*/
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/logout")
	public String logout(Model modello) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		authentication.setAuthenticated(false);
		return "homePage";
	}
	
	@GetMapping("/loginAllievo")
	public String loginAllievo() {
		return "loginAllievo";
	}
	
	@PreAuthorize("hasAnyRole('responsabile')")
	@GetMapping("/aggiuntaAttivita")
	public String aggiuntaAttivita() {
		return "aggiuntaAttivita";
	}
	
	@PreAuthorize("hasAnyRole('allievo')")
	@GetMapping("/homePageAllievo")
	public String homePageAllievo(Model modello) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(authentication.getName());
		Allievo allievoLog;
		List<Attivita> attivitaAllievo = new ArrayList<>();
		allievoLog = this.servAll.cercaPerNome(authentication.getName());
		for (Attivita attivitaLog : allievoLog.getAttivita()) {
			attivitaAllievo.add(attivitaLog);
		}
		modello.addAttribute("attivitaAllievo", attivitaAllievo);
		modello.addAttribute("nomeAllievo", allievoLog.getNome());
		return "homePageAllievo";
	}
	
	@PreAuthorize("hasAnyRole('responsabile')")
	@GetMapping("/homePageResponsabile")
	public String homePageResponsabile(Model modello) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Responsabile responsabileLog;
		Centro centroResp;
		responsabileLog = this.servResp.findByNome(authentication.getName());
		centroResp = responsabileLog.getCentro();
		modello.addAttribute("listaAttivita", centroResp.getAttivita());
		modello.addAttribute("nomeResp", responsabileLog.getNome());
		modello.addAttribute("nomeCentro", responsabileLog.getCentro().getNome());
		return "homePageResponsabile";
	}
	
	@PreAuthorize("hasAnyRole('allievo')")
	@GetMapping("/selezioneAttivita")
	public String selezioneAttivita(Model modello) {
		List<Attivita> attivitaTot = this.servAtt.cercaTutte();
		modello.addAttribute("attivitaTot", attivitaTot);
		return "selezioneAttivita";
	}
	
/*-------------------------------------------------------------*/
	
	@RequestMapping("/registrazioneAllievo")
	public String registrazioneAllievo(@RequestParam("nome") String nome, @RequestParam("cognome") String cognome, 
			@RequestParam("password") String password, @RequestParam("email") String email, @RequestParam("telefono") Long telefono,
			@RequestParam("citta") String citta, @RequestParam("data") String data, @RequestParam("username") String username,
			Model modello){

		Set<Ruolo> ruoli = new HashSet<>();
		Ruolo r = new Ruolo("allievo");
		this.servR.salvaRuolo(r);
		
		ruoli.add(r);
		
		List<Attivita> attivita = new ArrayList<>();
		
		Allievo allievo = new Allievo(1, cognome, nome, ruoli, citta, email, password, data, telefono, attivita);
		
		this.servAll.salvaAllievo(allievo);
		
		return "homePage";
	}
	
	@PreAuthorize("hasAnyRole('responsabile')")
	@GetMapping("/registrazioneAttivita")
	public String registrazioneAttivita(@RequestParam("nome") String nome, @RequestParam("data") String data, @RequestParam("ora") String ora, Model modello) {

		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Responsabile responsabile = this.servResp.findByNome(authentication.getName());
		List<Attivita> listaAttivita = responsabile.getCentro().getAttivita();
		List<Allievo> allievi = new ArrayList<>();
		Attivita attivita = new Attivita(nome, data, ora, allievi);
		listaAttivita.add(attivita);
		responsabile.getCentro().setAttivita(listaAttivita);
		this.servAtt.salvaAttivita(attivita);
		return homePageResponsabile(modello);	
		
	}
	
	@PreAuthorize("hasAnyRole('allievo')")
	@GetMapping("/attivitaSelezionate")
	public String attivitaSelezionate(Model modello, @RequestParam(name="check", required=true) String[] attivita) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Allievo allievoLog = this.servAll.cercaPerNome(authentication.getName());
		List<Attivita> listaAttivita = new ArrayList<>();
		for(String s : attivita) {
			listaAttivita.add(this.servAtt.cercaAttivita(s));
			this.servAtt.cercaAttivita(s).getAllievi().add(allievoLog);
		}
		allievoLog.getAttivita().addAll(listaAttivita);
		return homePageAllievo(modello);
	}
	
	@PreAuthorize("hasAnyRole('allievo')")
	@GetMapping("/disiscrizioneAttivita")
	public String eliminaAttivita(Model model) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Allievo allievo = this.servAll.cercaPerNome(authentication.getName());
		List<Attivita> attivitaAllievo = allievo.getAttivita();
		model.addAttribute("attivitaAllievo", attivitaAllievo);
		return "disiscrizioneAttivita";
	}
	
	@PreAuthorize("hasAnyRole('allievo')")
	@GetMapping("/attivitaDisiscritte")
	public String attivitaDisiscritte(Model modello, @RequestParam(name="check", required=true) String[] attivitaDis) {
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Allievo allievo = this.servAll.cercaPerNome(authentication.getName());
		List<Attivita> listaAttivita = allievo.getAttivita();
		Attivita a;
		for(String s : attivitaDis) {
			a=this.servAtt.cercaAttivita(s);
			listaAttivita.remove(a);
			a.getAllievi().remove(allievo);
		}
		return homePageAllievo(modello);
	}
}
